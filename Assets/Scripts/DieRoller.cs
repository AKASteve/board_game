﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DieRoller : MonoBehaviour {

	public int[] diceValues = new int[4];
	public int diceTotal;

	public Sprite[] DiceImageOne;
	public Sprite[] DiceImageZero;

	public bool IsDoneRolling = false;

	public void NewTurn() {
		// This is the start of a player's turn.
		// we don't have a roll for them yet.
		IsDoneRolling = false;
	}

	public void RollTheDice() {

		// In Ur, you roll four dice which have half of their faces with a value of "1"
		// and half have a value of "0"
		diceTotal = 0;
		for (int i = 0; i < diceValues.Length; i++) {
			diceValues[i] = Random.Range(0, 2);
			diceTotal += diceValues[i];
		}

		// Update the visuals to show the dice roll
		// TODO: This could include animations

		// We have 4 children, each is an image of the die. So grab that
		// child, and update its Image component to use the correct sprite

		for (int i = 0; i < 4; i++) {
			if (diceValues[i] == 0) {
				this.transform.GetChild(i).GetComponent<Image>().sprite = 
					DiceImageZero[Random.Range(0, DiceImageZero.Length)];
			} else {
				this.transform.GetChild(i).GetComponent<Image>().sprite = 
					DiceImageOne[Random.Range(0, DiceImageZero.Length)];
			}
		}

		IsDoneRolling = true;
	}
}
