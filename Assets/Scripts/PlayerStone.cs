﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStone : MonoBehaviour {

	public Square StartingSquare;
	Square currentSquare;

	DieRoller theRoller;

	// Use this for initialization
	void Start () {
		theRoller = GameObject.FindObjectOfType<DieRoller>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnMouseUp() {
		// TODO: Is the mouse over a UI element? If so, ignore the click.
		Debug.Log("Click");

		// Have we rolled the dice?
		if (theRoller.IsDoneRolling == false) {
			// We can't move yet.
			return;
		}

		int spacesToMove = theRoller.diceTotal;

		// Where should we end up?
		Square finalSquare = currentSquare;

		Debug.Log("Moving: "+spacesToMove.ToString());
		for (int i = 0; i < spacesToMove; i++) {
			if (finalSquare == null) {
				finalSquare = StartingSquare;
			} else {
				if (finalSquare.NextSquares == null || finalSquare.NextSquares.Length == 0) {
					// TODO: We have reached the end and must score.
					Debug.Log("SCORE!");
					Destroy(gameObject);
					return;
				} else if (finalSquare.NextSquares.Length > 1) {
					// TODO: Branch based on player id
					finalSquare = finalSquare.NextSquares[0];
				} else {
					finalSquare = finalSquare.NextSquares[0];
				}
			}
		}

		if (finalSquare == null) {
			return;
		}

		// Teleport the piece to the final square
		// TODO: ANIMATE
		this.transform.position = finalSquare.transform.position;
		currentSquare = finalSquare;
	}
}
