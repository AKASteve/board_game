﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiceTotalDisplay : MonoBehaviour {
		
	public DieRoller roller;

	void Start() {
		roller = GameObject.FindObjectOfType<DieRoller>();
	}

	// Update is called once per frame
	void Update () {
		if (roller.IsDoneRolling == false) {
			GetComponent<Text>().text = "= ?";
		} else {
			GetComponent<Text>().text = "= " + roller.diceTotal.ToString();
		}
	}
}
